//
//  ViewController.swift
//  ATHealthKit
//
//  Created by Dejan on 27/11/2018.
//  Copyright © 2018 agostini.tech. All rights reserved.
//

import UIKit
import HealthKit

fileprivate enum ATError: Error {
    case notAvailable, missingType
}

class ViewController: UIViewController {

    @IBOutlet weak var dobLabel: UILabel!
    @IBOutlet weak var sexLabel: UILabel!
    @IBOutlet weak var energyLabel: UILabel!
    @IBOutlet weak var waterLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        authorize { (success, error) in
            print("HK Authorization finished - success: \(success); error: \(error)")
            self.readCharacteristicsData()
            self.readEnergy()
            self.readWater()
        }
    }

    private func authorize(completion: @escaping (Bool, Error?) -> Void) {
        guard HKHealthStore.isHealthDataAvailable() else {
            completion(false, ATError.notAvailable)
            return
        }
        
        guard
            let dob = HKObjectType.characteristicType(forIdentifier: .dateOfBirth),
            let sex = HKObjectType.characteristicType(forIdentifier: .biologicalSex),
            let energy = HKObjectType.quantityType(forIdentifier: .activeEnergyBurned),
            let water = HKObjectType.quantityType(forIdentifier: .dietaryWater) else {
                completion(false, ATError.missingType)
                return
        }
        
        let writing: Set<HKSampleType> = [water]
        let reading: Set<HKObjectType> = [dob, sex, energy, water]
        
        HKHealthStore().requestAuthorization(toShare: writing, read: reading, completion: completion)
    }
    
    private func readCharacteristicsData() {
        let store = HKHealthStore()
        do {
            let dobComponents = try store.dateOfBirthComponents()
            let sex = try store.biologicalSex().biologicalSex
            
            DispatchQueue.main.async {
                self.dobLabel.text = "DOB: \(dobComponents.day!)/\(dobComponents.month!)/\(dobComponents.year!)"
                self.sexLabel.text = "Sex: \(sex.rawValue)"
            }
        } catch {
            print("Something went wrong: \(error)")
        }
    }
    
    private func readEnergy() {
        guard let energyType = HKSampleType.quantityType(forIdentifier: .activeEnergyBurned) else {
            print("Sample type not available")
            return
        }
        
        let last24hPredicate = HKQuery.predicateForSamples(withStart: Date().oneDayAgo, end: Date(), options: .strictEndDate)
        
        let energyQuery = HKSampleQuery(sampleType: energyType,
                                        predicate: last24hPredicate,
                                        limit: HKObjectQueryNoLimit,
                                        sortDescriptors: nil) {
                                            (query, sample, error) in
                                            
                                            guard
                                                error == nil,
                                                let quantitySamples = sample as? [HKQuantitySample] else {
                                                    print("Something went wrong: \(error)")
                                                    return
                                            }
                                            
                                            let total = quantitySamples.reduce(0.0) { $0 + $1.quantity.doubleValue(for: HKUnit.kilocalorie()) }
                                            print("Total kcal: \(total)")
                                            DispatchQueue.main.async {
                                                self.energyLabel.text = String(format: "Energy: %.2f", total)
                                            }
        }
        HKHealthStore().execute(energyQuery)
    }
    
    private func readWater() {
        guard let waterType = HKSampleType.quantityType(forIdentifier: .dietaryWater) else {
            print("Sample type not available")
            return
        }
        
        let last24hPredicate = HKQuery.predicateForSamples(withStart: Date().oneDayAgo, end: Date(), options: .strictEndDate)
        
        let waterQuery = HKSampleQuery(sampleType: waterType,
                                       predicate: last24hPredicate,
                                       limit: HKObjectQueryNoLimit,
                                       sortDescriptors: nil) {
                                        (query, samples, error) in
                                        
                                        guard
                                            error == nil,
                                            let quantitySamples = samples as? [HKQuantitySample] else {
                                                print("Something went wrong: \(error)")
                                                return
                                        }
                                        
                                        let total = quantitySamples.reduce(0.0) { $0 + $1.quantity.doubleValue(for: HKUnit.literUnit(with: .milli)) }
                                        print("total water: \(total)")
                                        DispatchQueue.main.async {
                                            self.waterLabel.text = String(format: "Water: %.2f", total)
                                        }
        }
        HKHealthStore().execute(waterQuery)
    }
    
    @IBAction func writeWater() {
        guard let waterType = HKSampleType.quantityType(forIdentifier: .dietaryWater) else {
            print("Sample type not available")
            return
        }
        
        let waterQuantity = HKQuantity(unit: HKUnit.literUnit(with: .milli), doubleValue: 200.0)
        let today = Date()
        let waterQuantitySample = HKQuantitySample(type: waterType, quantity: waterQuantity, start: today, end: today)
        
        HKHealthStore().save(waterQuantitySample) { (success, error) in
            print("HK write finished - success: \(success); error: \(error)")
            self.readWater()
        }
    }
}

extension Date {
    var oneDayAgo: Date {
        return self.addingTimeInterval(-86400)
    }
}

